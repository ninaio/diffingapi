﻿using System;
using System.IO;
using System.Reflection;
using DiffingApi.Models;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Swashbuckle.AspNetCore.Swagger;

namespace DiffingApi
{
    /// <summary>
    /// A Startup instance configures services and the app's request pipeline
    /// </summary>
    public class Startup
    {
        public Startup(IConfiguration configuration, bool isInTestMode = false)
        {
            Configuration = configuration;
            IsInTestMode = isInTestMode;
        }

        public bool IsInTestMode { get; set; }
        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            // Define database to use
            if (IsInTestMode)
            {
                // Set to true when Startup is called from 
                // DiffingApi.Tests.Integration.CustomWebApplicationFactory<TStartup>
                services.AddDbContext<DiffingDbContext>(options => options.UseInMemoryDatabase("DiffingDb"));
            }
            else
            {
                services.AddDbContext<DiffingDbContext>(opt => opt.UseSqlServer(Configuration.GetConnectionString("DiffingDatabase")));
            }

            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);

            // Set up Swagger
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new Info
                {
                    Title = "Diffing API",
                    Version = "v1",
                    Description = "A simple web api accepting JSON containing base64 encoded binary data on two endpoints (left and right) and displaying the difference results on a third endpoint",
                    Contact = new Contact
                    {
                        Name = "Nina Vujkov",
                        Email = "nina.vujkov@outlook.com"
                    },
                });

                var xmlFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
                var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);
                c.IncludeXmlComments(xmlPath);
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            // Instruct the API to use Swagger UI
            app.UseSwagger();

            app.UseSwaggerUI(c =>
            {
                c.RoutePrefix = string.Empty;
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "Diffing API");
            });


            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseHsts();
            }

            app.UseHttpsRedirection();
            app.UseMvc();
        }
    }
}
