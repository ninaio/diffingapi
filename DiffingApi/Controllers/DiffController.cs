﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DiffingApi.Models;
using DiffingApi.Models.Entities;
using DiffingApi.Models.Enums;
using Microsoft.AspNetCore.Mvc;

namespace DiffingApi.Controllers
{
    /// <summary>
    /// A DiffController instance handles requests defined in controller methods
    /// </summary>
    [Route("v1/diff")]
    [ApiController]
    public class DiffController : ControllerBase
    {
        private readonly DiffingDbContext _context;

        public DiffController(DiffingDbContext context)
        {
            _context = context;
        }

        #region Endpoints

        /// <summary>
        /// Creates diffing data row in DiffingDb using a left endpoint
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     PUT /v1/diff/1/left 
        ///     {
        ///        "data" : "SSB3YW50" 
        ///     }
        ///
        /// </remarks>
        /// <param name="id"></param>
        /// <param name="data"></param>
        /// <returns>Status code Created or Bad request</returns>
        /// <response code="201">Returns the Created response</response>
        /// <response code="400">If the item is null</response> 
        /// <response code="500">If an exception occurs</response>
        [ProducesResponseType(201)]
		[ProducesResponseType(400)]
        [ProducesResponseType(500)]
        [HttpPut("{id}/left")]
        public async Task<IActionResult> CreateLeft(long id, [FromBody] DataModel data)
        {
            return await CreateData(id, data, true);
        }

        /// <summary>
        /// Creates diffing data row in DiffingDb using a right endpoint
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     PUT /v1/diff/2/right 
        ///     {
        ///        "data" : "dGhpcyBqb2I=" 
        ///     }
        ///
        /// </remarks>
        /// <param name="id"></param>
        /// <param name="data"></param>
        /// <returns>Status code Created or Bad request</returns>
        /// <response code="201">Returns the Created response</response>
        /// <response code="400">If the item is null</response> 
        /// <response code="500">If an exception occurs</response>
        [ProducesResponseType(201)]
		[ProducesResponseType(400)]
        [ProducesResponseType(500)]
        [HttpPut("{id}/right")]
        public async Task<IActionResult> CreateRight(long id, [FromBody] DataModel data)
        {
            return await CreateData(id, data, false);
        }

        /// <summary>
        /// Creates diffing result and differences rows in DiffingDb if they don't exist or reads an existing one
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     GET /v1/diff/1
        ///
        /// </remarks>
        /// <param name="id">The id of the left and the right data that need to be diffed</param>
        /// <returns>Status code OK or Not found</returns>
        /// <response code="200">Returns the difference between the left and right data</response>
        /// <response code="404">If left or right data does not exist</response> 
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [HttpGet("{id}")]
        public async Task<IActionResult> GetDiff(long id)
        {
            return await CompareData(id);
        }

        #endregion Endpoints

        #region Private methods - IgnoreApi = true

        /// <summary>
        /// Creates diffing data row in DiffingDb
        /// </summary>
        /// <param name="id">The value of the DataId field of the DataEntity</param>
        /// <param name="data">DataModel from JSON</param>
        /// <param name="isLeft">The side of the data</param>
        /// <returns></returns>
        private async Task<IActionResult> CreateData(long id, DataModel data, bool isLeft)
        {
            try
            {
                // Check if data is valid
                if(!data.Data.IsBase64String())
                {
                    return BadRequest("The provided data is not a base 64 string");
                }
                
                DataEntity dataForDatabase = new DataEntity()
                {
                    DataId = id,
                    Data = Convert.FromBase64String(data.Data),
                    IsLeft = isLeft
                };

                // Check if the data exists. If it does - update
                var existingData = _context.Datas.FindByDataIdAndSide(id, isLeft);

                if(existingData != null)
                {
                    existingData.Data = dataForDatabase.Data;
                    _context.Datas.Update(existingData);

                    // Delete related results and differences so the updated data is compared again when GetDiff is called
                    var resultToRemove = _context.DiffResults.Find(existingData.Id);

                    if(resultToRemove != null)
                    {
                        // If the ResultType is ContentDoNotMatch it means there are also 
                        // differences stored, which need to be deleted, too
                        if(resultToRemove.ResultType == ResultType.ContentDoNotMatch)
                        {
                            var differencesToRemove = _context.Differences.Where(o => o.ResultId == existingData.Id);

                            // Differences have ResultId as foreign key, so they need to be removed first
                            if (differencesToRemove != null)
                            {
                                _context.Differences.RemoveRange(differencesToRemove);
                            }
                        }

                        _context.DiffResults.Remove(resultToRemove);
                    }
                }
                else
                {
                    _context.Datas.Add(dataForDatabase);
                }

                int result = await _context.SaveChangesAsync();

                // If Success
                if (result > 0)
                {
                    return StatusCode(201);
                }
                return StatusCode(500);
            }
            catch
            {
                return StatusCode(500);
            }
        }

        /// <summary>
        /// Compares two datas from DiffingDb and returns the result
        /// </summary>
        /// <param name="id">Id of the left and the right data to compare</param>
        /// <returns>The result of the comparaison along with a list of differences</returns>
        private async Task<IActionResult> CompareData(long id)
        {
            try
            {
                // Check if the difference already exist in the database. If it does - return that
                var existingDiff = await _context.DiffResults.FindAsync(id);

                if (existingDiff != null)
                {
                    return Ok(existingDiff.MapToModel());
                }
                
                var leftData = _context.Datas.FindByDataIdAndSide(id, true);
                var rightData = _context.Datas.FindByDataIdAndSide(id, false);

                if (leftData == null || rightData == null)
                {
                    return NotFound();
                }

                DiffResultEntity diffResult = new DiffResultEntity()
                {
                    Id = id
                };

                List<DifferenceEntity> differences = new List<DifferenceEntity>();

                if (leftData.Data.SequenceEqual(rightData.Data))
                {
                    diffResult.ResultType = ResultType.Equals;
                }
                else if (leftData.Data.Count() != rightData.Data.Count())
                {
                    diffResult.ResultType = ResultType.SizeDoNotMatch;
                }
                else
                {
                    diffResult.ResultType = ResultType.ContentDoNotMatch;
                    differences = CalculateDifferences(leftData.Data, rightData.Data);

                    // Assign current result
                    differences.ForEach(d =>
                    {
                        d.Result = diffResult;
                        d.ResultId = id;
                    });
                }

                _context.DiffResults.Add(diffResult);

                // If contents don't match - it means there are differences to store
                if (diffResult.ResultType == ResultType.ContentDoNotMatch)
                {
                    _context.Differences.AddRange(differences);
                }
                
                int result = await _context.SaveChangesAsync();

                if (result > 0)
                {
                    // Create model for response
                    DiffResultModel model = diffResult.MapToModel();
                    model.Diffs = differences.MapToModel();

                    return Ok(model);
                }

                return StatusCode(500);
            }
            catch
            {
                return StatusCode(500);
            }
        }

        /// <summary>
        /// Calculates the differences between two decoded datas
        /// </summary>
        /// <param name="leftData">Decoded base64 string</param>
        /// <param name="rightData">Decoded base64 string</param>
        /// <returns>A list of differences (lengths and offsets) between the two datas</returns>
        private List<DifferenceEntity> CalculateDifferences(byte[] leftData, byte[] rightData)
        {
            try
            {
                List<DifferenceEntity> differences = new List<DifferenceEntity>();
                DifferenceEntity currentDifference = null;

                for (int i = 0; i < leftData.Count(); i++)
                {
                    // If the datas match, that means the previous 
                    // difference has ended - reset it to null
                    if (leftData[i] == rightData[i])
                    {
                        if (currentDifference != null)
                        {
                            currentDifference = null;
                        }

                        continue;
                    }
                    // If the datas don't match current difference
                    // is still in progress - increase length or create it
                    else
                    {
                        if (currentDifference == null)
                        {
                            currentDifference = new DifferenceEntity()
                            {
                                Offset = i,
                                Length = 1
                            };

                            differences.Add(currentDifference);
                        }
                        else
                        {
                            currentDifference.Length++;
                        }
                    }
                }

                return differences;
            }
            catch
            {
                return null;
            }
        }

        #endregion Private methods - IgnoreApi = true
    }
}
