<?xml version="1.0"?>
<doc>
    <assembly>
        <name>DiffingApi</name>
    </assembly>
    <members>
        <member name="T:DiffingApi.Controllers.DiffController">
            <summary>
            A DiffController instance handles requests defined in controller methods
            </summary>
        </member>
        <member name="M:DiffingApi.Controllers.DiffController.CreateLeft(System.Int64,DiffingApi.Models.DataModel)">
             <summary>
             Creates diffing data row in DiffingDb using a left endpoint
             </summary>
             <remarks>
             Sample request:
            
                 PUT /v1/diff/1/left 
                 {
                    "data" : "SSB3YW50" 
                 }
            
             </remarks>
             <param name="id"></param>
             <param name="data"></param>
             <returns>Status code Created or Bad request</returns>
             <response code="201">Returns the Created response</response>
             <response code="400">If the item is null</response> 
             <response code="500">If an exception occurs</response>
        </member>
        <member name="M:DiffingApi.Controllers.DiffController.CreateRight(System.Int64,DiffingApi.Models.DataModel)">
             <summary>
             Creates diffing data row in DiffingDb using a right endpoint
             </summary>
             <remarks>
             Sample request:
            
                 PUT /v1/diff/2/right 
                 {
                    "data" : "dGhpcyBqb2I=" 
                 }
            
             </remarks>
             <param name="id"></param>
             <param name="data"></param>
             <returns>Status code Created or Bad request</returns>
             <response code="201">Returns the Created response</response>
             <response code="400">If the item is null</response> 
             <response code="500">If an exception occurs</response>
        </member>
        <member name="M:DiffingApi.Controllers.DiffController.GetDiff(System.Int64)">
             <summary>
             Creates diffing result and differences rows in DiffingDb if they don't exist or reads an existing one
             </summary>
             <remarks>
             Sample request:
            
                 GET /v1/diff/1
            
             </remarks>
             <param name="id">The id of the left and the right data that need to be diffed</param>
             <returns>Status code OK or Not found</returns>
             <response code="200">Returns the difference between the left and right data</response>
             <response code="404">If left or right data does not exist</response> 
        </member>
        <member name="M:DiffingApi.Controllers.DiffController.CreateData(System.Int64,DiffingApi.Models.DataModel,System.Boolean)">
            <summary>
            Creates diffing data row in DiffingDb
            </summary>
            <param name="id">The value of the DataId field of the DataEntity</param>
            <param name="data">DataModel from JSON</param>
            <param name="isLeft">The side of the data</param>
            <returns></returns>
        </member>
        <member name="M:DiffingApi.Controllers.DiffController.CompareData(System.Int64)">
            <summary>
            Compares two datas from DiffingDb and returns the result
            </summary>
            <param name="id">Id of the left and the right data to compare</param>
            <returns>The result of the comparaison along with a list of differences</returns>
        </member>
        <member name="M:DiffingApi.Controllers.DiffController.CalculateDifferences(System.Byte[],System.Byte[])">
            <summary>
            Calculates the differences between two decoded datas
            </summary>
            <param name="leftData">Decoded base64 string</param>
            <param name="rightData">Decoded base64 string</param>
            <returns>A list of differences (lengths and offsets) between the two datas</returns>
        </member>
        <member name="T:DiffingApi.Models.DataModel">
            <summary>
            An instance of DataModel class represents Data that is forwarded using JSON
            </summary>
        </member>
        <member name="P:DiffingApi.Models.DataModel.Data">
            <summary>
            Base64 encoded data
            </summary>
        </member>
        <member name="T:DiffingApi.Models.DifferenceModel">
            <summary>
            An instance of DifferenceModel class represents one Difference (offset and length)
            that is forwarded using JSON
            </summary>
        </member>
        <member name="P:DiffingApi.Models.DifferenceModel.Offset">
            <summary>
            The position of the beginning of the difference
            </summary>
        </member>
        <member name="P:DiffingApi.Models.DifferenceModel.Length">
            <summary>
            The length of the difference
            </summary>
        </member>
        <member name="T:DiffingApi.Models.DiffingDbContext">
            <summary>
            A context class defining DiffingDb
            </summary>
        </member>
        <member name="T:DiffingApi.Models.DiffResultModel">
            <summary>
            An instance of DiffingResultModel class represents the result of
            the action of diffing. It contains the result type and a list
            od differences described in DifferenceModel class
            </summary>
        </member>
        <member name="P:DiffingApi.Models.DiffResultModel.ResultType">
            <summary>
            Equals, SizeDoNotMatch or ContentDoNotMatch
            </summary>
        </member>
        <member name="P:DiffingApi.Models.DiffResultModel.Diffs">
            <summary>
            A list of all found differences defined by length and offset
            </summary>
        </member>
        <member name="T:DiffingApi.Models.Entities.DataEntity">
            <summary>
            Entity representing Datas table row in DiffingDb
            </summary>
        </member>
        <member name="P:DiffingApi.Models.Entities.DataEntity.Id">
            <summary>
            Primary key, identity, automatically calculated
            </summary>
        </member>
        <member name="P:DiffingApi.Models.Entities.DataEntity.DataId">
            <summary>
            Id of the data set up by client in Controller request
            </summary>
        </member>
        <member name="P:DiffingApi.Models.Entities.DataEntity.Data">
            <summary>
            Decoded data
            </summary>
        </member>
        <member name="P:DiffingApi.Models.Entities.DataEntity.IsLeft">
            <summary>
            Side of the data set up by client in Controller request
            </summary>
        </member>
        <member name="T:DiffingApi.Models.Entities.DifferenceEntity">
            <summary>
            Entity representing Differences table row in DiffingDb
            </summary>
        </member>
        <member name="P:DiffingApi.Models.Entities.DifferenceEntity.Id">
            <summary>
            Primary key, identity, automatically calculated
            </summary>
        </member>
        <member name="P:DiffingApi.Models.Entities.DifferenceEntity.ResultId">
            <summary>
            Foreign key from DiffResult table
            </summary>
        </member>
        <member name="P:DiffingApi.Models.Entities.DifferenceEntity.Offset">
            <summary>
            The position where the difference occured
            </summary>
        </member>
        <member name="P:DiffingApi.Models.Entities.DifferenceEntity.Length">
            <summary>
            The length of the difference
            </summary>
        </member>
        <member name="P:DiffingApi.Models.Entities.DifferenceEntity.Result">
            <summary>
            The DiffingResult containing this difference with the Id - ResultId
            </summary>
        </member>
        <member name="T:DiffingApi.Models.Entities.DiffResultEntity">
            <summary>
            Entity representing DiffResults table row in DiffingDb
            </summary>
        </member>
        <member name="P:DiffingApi.Models.Entities.DiffResultEntity.Id">
            <summary>
            Primary key, set by the user when requesting the difference
            if left and right data with the same DataId already exist
            </summary>
        </member>
        <member name="P:DiffingApi.Models.Entities.DiffResultEntity.ResultType">
            <summary>
            Equals, SizeDoNotMatch, ContentDoNotMatch
            </summary>
        </member>
        <member name="P:DiffingApi.Models.Entities.DiffResultEntity.Differences">
            <summary>
            A collection of differences for the given result
            if ResultType is ContentDoNotMatch
            </summary>
        </member>
        <member name="T:DiffingApi.Models.Enums.ResultType">
            <summary>
            Enum describing the result type
            </summary>
        </member>
        <member name="F:DiffingApi.Models.Enums.ResultType.Equals">
            <summary>
            The two datas are equal
            </summary>
        </member>
        <member name="F:DiffingApi.Models.Enums.ResultType.SizeDoNotMatch">
            <summary>
            The two datas are not of the same size
            </summary>
        </member>
        <member name="F:DiffingApi.Models.Enums.ResultType.ContentDoNotMatch">
            <summary>
            The two datas are of the same size but their contents do not match
            </summary>
        </member>
        <member name="T:DiffingApi.Models.Extensions">
            <summary>
            Helper class to extend methods of certain types
            </summary>
        </member>
        <member name="M:DiffingApi.Models.Extensions.FindByDataIdAndSide(System.Collections.Generic.IEnumerable{DiffingApi.Models.Entities.DataEntity},System.Int64,System.Boolean)">
            <summary>
            Uses Linq to find DataEntity in a list of DataEntities
            that has a specific id and is placed on left or right side
            </summary>
            <param name="datas">A list of DataEntities which will be searched</param>
            <param name="id">An id of the data to find</param>
            <param name="isLeft">The side of the data to find. True for left</param>
            <returns>One DataEntity that matched the criteria or null</returns>
        </member>
        <member name="M:DiffingApi.Models.Extensions.MapToModel(DiffingApi.Models.Entities.DiffResultEntity)">
            <summary>
            Maps DiffResultEntity to DiffResultModel
            </summary>
            <param name="entity">A DiffResultEntity instance to be mapped</param>
            <returns>DiffResultModel derived from DiffResultEntity</returns>
        </member>
        <member name="M:DiffingApi.Models.Extensions.MapToModel(System.Collections.Generic.IEnumerable{DiffingApi.Models.Entities.DifferenceEntity})">
            <summary>
            Maps a list of DifferenceEntities to a list of DifferenceModels
            </summary>
            <param name="entities">A list of DifferenceEntities to be mapped</param>
            <returns>A list of DifferenceModels</returns>
        </member>
        <member name="M:DiffingApi.Models.Extensions.IsNullOrEmpty(System.Collections.Generic.IEnumerable{System.Object})">
            <summary>
            Checks if a list of objects is empty or null
            </summary>
            <param name="list">A list to be checked</param>
            <returns>True if empty or null, false otherwise</returns>
        </member>
        <member name="M:DiffingApi.Models.Extensions.IsBase64String(System.String)">
            <summary>
            Uses Regex to check whether a string is a valid base64 encoded string
            </summary>
            <param name="base64String">A string to be validated</param>
            <returns>True if it is a valid string, false otherwise</returns>
        </member>
        <member name="T:DiffingApi.Program">
            <summary>
            The apps entry point
            </summary>
        </member>
        <member name="T:DiffingApi.Startup">
            <summary>
            A Startup instance configures services and the app's request pipeline
            </summary>
        </member>
    </members>
</doc>
