﻿using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;

namespace DiffingApi
{
    /// <summary>
    /// The apps entry point
    /// </summary>
    public class Program
    {
        public static void Main(string[] args)
        {
            // Configures Kestrel server as the web server and loads configuration
            CreateWebHostBuilder(args).Build().Run();
        }

        public static IWebHostBuilder CreateWebHostBuilder(string[] args) =>
            WebHost.CreateDefaultBuilder(args).UseStartup<Startup>();
    }
}
