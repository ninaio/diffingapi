﻿using System.ComponentModel.DataAnnotations;
namespace DiffingApi.Models
{
    /// <summary>
    /// An instance of DataModel class represents Data that is forwarded using JSON
    /// </summary>
    public class DataModel
    {
        /// <summary>
        /// Base64 encoded data
        /// </summary>
        [Required]
        public string Data { get; set; }
    }
}
