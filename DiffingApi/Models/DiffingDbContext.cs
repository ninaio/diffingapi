﻿using DiffingApi.Models.Entities;
using Microsoft.EntityFrameworkCore;

namespace DiffingApi.Models
{
    /// <summary>
    /// A context class defining DiffingDb
    /// </summary>
    public partial class DiffingDbContext : DbContext
    {
        public DiffingDbContext()
        {
        }

        public DiffingDbContext(DbContextOptions<DiffingDbContext> options)
            : base(options)
        {
        }

        // List of all tables in the database
        public virtual DbSet<DataEntity> Datas { get; set; }
        public virtual DbSet<DifferenceEntity> Differences { get; set; }
        public virtual DbSet<DiffResultEntity> DiffResults { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            // Setting up constraints
            modelBuilder.Entity<DataEntity>(entity =>
            {
                entity.Property(e => e.Data).IsRequired();
            });

            modelBuilder.Entity<DifferenceEntity>(entity =>
            {
                entity.HasOne(d => d.Result)
                    .WithMany(p => p.Differences)
                    .HasForeignKey(d => d.ResultId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Differences_DiffResults");
            });
        }
    }
}
