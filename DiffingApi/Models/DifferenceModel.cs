﻿namespace DiffingApi.Models
{
    /// <summary>
    /// An instance of DifferenceModel class represents one Difference (offset and length)
    /// that is forwarded using JSON
    /// </summary>
    public class DifferenceModel
    {
        /// <summary>
        /// The position of the beginning of the difference
        /// </summary>
        public long Offset { get; set; }

        /// <summary>
        /// The length of the difference
        /// </summary>
        public long Length { get; set; }
    }
}
