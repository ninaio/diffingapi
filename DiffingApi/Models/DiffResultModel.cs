﻿using DiffingApi.Models.Enums;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System.Collections.Generic;

namespace DiffingApi.Models
{
    /// <summary>
    /// An instance of DiffingResultModel class represents the result of
    /// the action of diffing. It contains the result type and a list
    /// od differences described in DifferenceModel class
    /// </summary>
    public class DiffResultModel
    {
        /// <summary>
        /// Equals, SizeDoNotMatch or ContentDoNotMatch
        /// </summary>
        [JsonConverter(typeof(StringEnumConverter))]
        public ResultType ResultType { get; set; }

        /// <summary>
        /// A list of all found differences defined by length and offset
        /// </summary>
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public IEnumerable<DifferenceModel> Diffs { get; set; }
    }
}
