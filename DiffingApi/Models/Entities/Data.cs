﻿namespace DiffingApi.Models.Entities
{
    /// <summary>
    /// Entity representing Datas table row in DiffingDb
    /// </summary>
    public partial class DataEntity
    {
        /// <summary>
        /// Primary key, identity, automatically calculated
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// Id of the data set up by client in Controller request
        /// </summary>
        public long DataId { get; set; }

        /// <summary>
        /// Decoded data
        /// </summary>
        public byte[] Data { get; set; }

        /// <summary>
        /// Side of the data set up by client in Controller request
        /// </summary>
        public bool IsLeft { get; set; }
    }
}
