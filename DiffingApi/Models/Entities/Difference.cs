﻿namespace DiffingApi.Models.Entities
{
    /// <summary>
    /// Entity representing Differences table row in DiffingDb
    /// </summary>
    public partial class DifferenceEntity
    {
        /// <summary>
        /// Primary key, identity, automatically calculated
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// Foreign key from DiffResult table
        /// </summary>
        public long ResultId { get; set; }

        /// <summary>
        /// The position where the difference occured
        /// </summary>
        public long Offset { get; set; }

        /// <summary>
        /// The length of the difference
        /// </summary>
        public long Length { get; set; }

        /// <summary>
        /// The DiffingResult containing this difference with the Id - ResultId
        /// </summary>
        public DiffResultEntity Result { get; set; }
    }
}
