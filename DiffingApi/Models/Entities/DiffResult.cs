﻿using DiffingApi.Models.Enums;
using System.Collections.Generic;

namespace DiffingApi.Models.Entities
{
    /// <summary>
    /// Entity representing DiffResults table row in DiffingDb
    /// </summary>
    public partial class DiffResultEntity
    {
        public DiffResultEntity()
        {
            Differences = new HashSet<DifferenceEntity>();
        }

        /// <summary>
        /// Primary key, set by the user when requesting the difference
        /// if left and right data with the same DataId already exist
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// Equals, SizeDoNotMatch, ContentDoNotMatch
        /// </summary>
        public ResultType ResultType { get; set; }

        /// <summary>
        /// A collection of differences for the given result
        /// if ResultType is ContentDoNotMatch
        /// </summary>
        public ICollection<DifferenceEntity> Differences { get; set; }
    }
}
