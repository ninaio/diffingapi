﻿namespace DiffingApi.Models.Enums
{
    /// <summary>
    /// Enum describing the result type
    /// </summary>
    public enum ResultType
    {
        /// <summary>
        /// The two datas are equal
        /// </summary>
        Equals = 0,

        /// <summary>
        /// The two datas are not of the same size
        /// </summary>
        SizeDoNotMatch = 1,

        /// <summary>
        /// The two datas are of the same size but their contents do not match
        /// </summary>
        ContentDoNotMatch = 2
    }
}
