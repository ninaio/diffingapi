﻿using DiffingApi.Models.Entities;
using DiffingApi.Models.Enums;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace DiffingApi.Models
{
    /// <summary>
    /// Helper class to extend methods of certain types
    /// </summary>
    public static class Extensions
    {
        /// <summary>
        /// Uses Linq to find DataEntity in a list of DataEntities
        /// that has a specific id and is placed on left or right side
        /// </summary>
        /// <param name="datas">A list of DataEntities which will be searched</param>
        /// <param name="id">An id of the data to find</param>
        /// <param name="isLeft">The side of the data to find. True for left</param>
        /// <returns>One DataEntity that matched the criteria or null</returns>
        public static DataEntity FindByDataIdAndSide(this IEnumerable<DataEntity> datas, long id, bool isLeft)
        {
            return datas.Where(d => d.DataId == id && d.IsLeft == isLeft).FirstOrDefault();
        }

        /// <summary>
        /// Maps DiffResultEntity to DiffResultModel
        /// </summary>
        /// <param name="entity">A DiffResultEntity instance to be mapped</param>
        /// <returns>DiffResultModel derived from DiffResultEntity</returns>
        public static DiffResultModel MapToModel(this DiffResultEntity entity)
        {
            return new DiffResultModel()
            {
                ResultType = entity.ResultType,
                Diffs = entity.ResultType != ResultType.ContentDoNotMatch ? null : entity.Differences.MapToModel()
            };
        }

        /// <summary>
        /// Maps a list of DifferenceEntities to a list of DifferenceModels
        /// </summary>
        /// <param name="entities">A list of DifferenceEntities to be mapped</param>
        /// <returns>A list of DifferenceModels</returns>
        public static IEnumerable<DifferenceModel> MapToModel(this IEnumerable<DifferenceEntity> entities)
        {
            if(entities.Count() == 0)
            {
                return null;
            }

            List<DifferenceModel> modelDifferences = new List<DifferenceModel>();

            entities.ToList()
                .ForEach(d => 
                modelDifferences.Add(new DifferenceModel()
                {
                    Length = d.Length,
                    Offset = d.Offset
                }));

            return modelDifferences;
        }

        /// <summary>
        /// Checks if a list of objects is empty or null
        /// </summary>
        /// <param name="list">A list to be checked</param>
        /// <returns>True if empty or null, false otherwise</returns>
        public static bool IsNullOrEmpty(this IEnumerable<object> list)
        {
            return list == null || list.Count() == 0;
        }

        /// <summary>
        /// Uses Regex to check whether a string is a valid base64 encoded string
        /// </summary>
        /// <param name="base64String">A string to be validated</param>
        /// <returns>True if it is a valid string, false otherwise</returns>
        public static bool IsBase64String(this string base64String)
        {
            if(string.IsNullOrEmpty(base64String))
            {
                return false;
            }

            base64String = base64String.Trim();
            return (base64String.Length % 4 == 0) && Regex.IsMatch(base64String, @"^[a-zA-Z0-9\+/]*={0,3}$", RegexOptions.None);
        }
    }
}
