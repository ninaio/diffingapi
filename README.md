# DiffingApi web api

## Description

A simple application that returns a JSON describing the difference between the two base64 encoded binary data accepted over two endpoints.

## The task

* Provide 2 http endpoints (<host>/v1/diff/<ID>/left and <host>/v1/diff/<ID>/right) that accept JSON containing base64 encoded binary data on both endpoints.
* The provided data needs to be diff-ed and the results shall be available on a third endpoint (<host>/v1/diff/<ID>). 

The results shall provide the following info in JSON format:

* If equal return that 
* If not of equal size just return that 
* If of same size provide insight in where the diff are, actual diffs are not needed.
	* So mainly offsets + length in the data 

## Requirements

* Preferably C#. If you are really not comfortable with C#, you can use some other objectoriented and/or functional language, but provide more detailed info for running the solution 
* Functionality shall be under integration tests (not full code coverage is required) 
* Internal logic shall be under unit tests (not full code coverage is required) 
* Documentation in code 
* Short readme on usage

## The solution

The solution is developed using Visual Studio 2017, .NET Core RESTful api.
The main project is DiffingApi, which, upon starting creates a [Swagger UI](https://swagger.io/tools/swagger-ui/) which can be used to test the solution. Optionally, [Postman](https://www.getpostman.com/) can also be used for the same purposes.

The api uses SqlServer localDb named "DiffingDb", which is usually stored on the path C:/Users/Username. The testing project, on the other hand, uses an in-memory database, where the data is stored only while the process is run.

There are no specific actions to take, except starting the project.

The typical program flow is shown on the diagram below.

![Alt text](https://i.ibb.co/Ykfd07n/Program-flow.png "Program flow") 

## Packages

The following Nuget packages are used in this solution:

	Microsoft.AspNetCore.App 2.1.1
	Microsoft.AspNetCore.Mvc.Testing 2.1.3
	Microsoft.AspNetCore.Razor.Design 2.1.2
	Microsoft.NET.Test.Sdk 15.9.0
	Microsoft.NETCore.App 2.1.0
	Swashbuckle.AspNetCore 4.0.1
	xunit 2.4.0
	xunit.runner.visualstudio 2.4.0

Keep in mind that downloading packages might take some time when opening the solution in VisualStudio for the first time.