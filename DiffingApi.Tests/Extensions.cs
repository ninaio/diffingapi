﻿using Microsoft.AspNetCore.Mvc;
using System.Net;

namespace DiffingApi.Tests
{
    public static class Extensions
    {
        public static HttpStatusCode StatusCode(this IActionResult action)
        {
            return (HttpStatusCode)(action as StatusCodeResult).StatusCode;
        }
    }
}
