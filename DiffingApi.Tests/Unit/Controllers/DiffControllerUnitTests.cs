using DiffingApi.Controllers;
using DiffingApi.Models;
using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;
using Xunit;
using Microsoft.AspNetCore.Mvc;
using System.Net;
using DiffingApi.Models.Enums;
using System.Linq;

namespace DiffingApi.Tests.Unit.Controllers
{
    public class DiffControllerUnitTests
    {
        private DiffingDbContext _context;

        private DiffController CreateDiffController()
        {
            // Set up in memory database to be used with unit tests
            var optionsBuilder = new DbContextOptionsBuilder<DiffingDbContext>();
            optionsBuilder.UseInMemoryDatabase("DiffingDb");

            _context = new DiffingDbContext(optionsBuilder.Options);
            return new DiffController(_context);
        }

        #region Testing to pass

        [Fact]
        public async Task CanCreateLeft()
        {
            var unitUnderTest = CreateDiffController();

            long id = 1;
            DataModel data = new DataModel() { Data = "aW10aXJlZA==" };
            
            var result = await unitUnderTest.CreateLeft(id, data);
            Assert.Equal(HttpStatusCode.Created, result.StatusCode());
        }

        [Fact]
        public async Task CanCreateRight()
        {
            var unitUnderTest = CreateDiffController();

            long id = 1;
            DataModel data = new DataModel() { Data = "Y2FueW91cmVhZHRoaXM=" };

            var result = await unitUnderTest.CreateRight(id, data);
            Assert.Equal(HttpStatusCode.Created, result.StatusCode());
        }

        [Fact]
        public async Task CanUpdateLeft()
        {
            var unitUnderTest = CreateDiffController();

            long id = 1;
            DataModel data = new DataModel() { Data = "aW10aXJlZA==" };

            var result = await unitUnderTest.CreateLeft(id, data);
            Assert.Equal(HttpStatusCode.Created, result.StatusCode());

            DataModel updatedData = new DataModel() { Data = "Y2FueW91cmVhZHRoaXM=" };

            var updateResult = await unitUnderTest.CreateLeft(id, updatedData);
            Assert.Equal(HttpStatusCode.Created, updateResult.StatusCode());
        }

        [Fact]
        public async Task CanUpdateRight()
        {
            var unitUnderTest = CreateDiffController();

            long id = 1;
            DataModel data = new DataModel() { Data = "Y2FueW91cmVhZHRoaXM=" };

            var result = await unitUnderTest.CreateRight(id, data);
            Assert.Equal(HttpStatusCode.Created, result.StatusCode());

            DataModel updatedData = new DataModel() { Data = "aW10aXJlZA==" };

            var updateResult = await unitUnderTest.CreateRight(id, updatedData);
            Assert.Equal(HttpStatusCode.Created, updateResult.StatusCode());
        }

        [Fact]
        public async Task CanUpdateSameDataLeft()
        {
            var unitUnderTest = CreateDiffController();

            long id = 1;
            DataModel data = new DataModel() { Data = "aW10aXJlZA==" };

            var result = await unitUnderTest.CreateLeft(id, data);
            Assert.Equal(HttpStatusCode.Created, result.StatusCode());

            var updateResult = await unitUnderTest.CreateLeft(id, data);
            Assert.Equal(HttpStatusCode.Created, updateResult.StatusCode());
        }

        [Fact]
        public async Task CanUpdateSameDataRight()
        {
            var unitUnderTest = CreateDiffController();

            long id = 1;
            DataModel data = new DataModel() { Data = "Y2FueW91cmVhZHRoaXM=" };

            var result = await unitUnderTest.CreateRight(id, data);
            Assert.Equal(HttpStatusCode.Created, result.StatusCode());

            var updateResult = await unitUnderTest.CreateRight(id, data);
            Assert.Equal(HttpStatusCode.Created, updateResult.StatusCode());
        }

        [Fact]
        public async Task CanDeleteDiffResultIfDataUpdated()
        {
            var unitUnderTest = CreateDiffController();

            // Create left and right data
            long id = 1;

            DataModel leftData = new DataModel() { Data = "Y2FueW91cmVhZHRoaXM=" };
            var leftResult = await unitUnderTest.CreateLeft(id, leftData);
            Assert.Equal(HttpStatusCode.Created, leftResult.StatusCode());

            DataModel rightData = new DataModel() { Data = "Y2FueW91cmVhZHRoaXM=" };
            var rightResult = await unitUnderTest.CreateRight(id, rightData);
            Assert.Equal(HttpStatusCode.Created, rightResult.StatusCode());

            // Calculate the difference and write it to the database
            var result = await unitUnderTest.GetDiff(id);
            Assert.Equal(typeof(OkObjectResult), result.GetType());

            DiffResultModel diffResult = (DiffResultModel)(result as OkObjectResult).Value;
            Assert.Equal(ResultType.Equals, diffResult.ResultType);
            Assert.True(diffResult.Diffs.IsNullOrEmpty());
            Assert.True(_context.DiffResults.Count() == 1);

            // Update left data
            DataModel updatedData = new DataModel() { Data = "aG93YXJleW91" };

            var updateResult = await unitUnderTest.CreateLeft(id, updatedData);
            Assert.Equal(HttpStatusCode.Created, updateResult.StatusCode());

            // Check if the Diff result was removed from the database
            Assert.True(_context.DiffResults.Count() == 0);
        }

        [Fact]
        public async Task CanDeleteDiffResultAndDifferencesIfDataUpdated()
        {
            var unitUnderTest = CreateDiffController();

            // Create left and right data
            long id = 1;

            DataModel leftData = new DataModel() { Data = "dGhpcw==" }; // this
            var leftResult = await unitUnderTest.CreateLeft(id, leftData);
            Assert.Equal(HttpStatusCode.Created, leftResult.StatusCode());

            DataModel rightData = new DataModel() { Data = "dGhhdA==" }; // that
            var rightResult = await unitUnderTest.CreateRight(id, rightData);
            Assert.Equal(HttpStatusCode.Created, rightResult.StatusCode());

            // Calculate the difference and write it to the database
            var result = await unitUnderTest.GetDiff(id);
            Assert.Equal(typeof(OkObjectResult), result.GetType());

            DiffResultModel diffResult = (DiffResultModel)(result as OkObjectResult).Value;
            Assert.Equal(ResultType.ContentDoNotMatch, diffResult.ResultType);
            Assert.NotEmpty(diffResult.Diffs);
            Assert.True(diffResult.Diffs.ToList().Count() == 1);
            Assert.True(_context.DiffResults.Count() == 1);
            Assert.True(_context.Differences.Count() == 1);

            // Update left data
            DataModel updatedData = new DataModel() { Data = "aG93YXJleW91" };

            var updateResult = await unitUnderTest.CreateLeft(id, updatedData);
            Assert.Equal(HttpStatusCode.Created, updateResult.StatusCode());

            // Check if the Diff result and Differences were removed from the database
            Assert.True(_context.DiffResults.Count() == 0);
            Assert.True(_context.Differences.Count() == 0);
        }

        [Fact]
        public async Task CanGetDiffEqual()
        {
            var unitUnderTest = CreateDiffController();

            long id = 1;

            DataModel leftData = new DataModel() { Data = "Y2FueW91cmVhZHRoaXM=" };
            var leftResult = await unitUnderTest.CreateLeft(id, leftData);
            Assert.Equal(HttpStatusCode.Created, leftResult.StatusCode());
            
            DataModel rightData = new DataModel() { Data = "Y2FueW91cmVhZHRoaXM=" };
            var rightResult = await unitUnderTest.CreateRight(id, rightData);
            Assert.Equal(HttpStatusCode.Created, rightResult.StatusCode());

            var result = await unitUnderTest.GetDiff(id);
            Assert.Equal(typeof(OkObjectResult), result.GetType());

            DiffResultModel diffResult = (DiffResultModel)(result as OkObjectResult).Value;
            Assert.Equal(ResultType.Equals, diffResult.ResultType);
            Assert.True(diffResult.Diffs.IsNullOrEmpty());
        }

        [Fact]
        public async Task CanGetDiffSizeDoNotMatch()
        {
            var unitUnderTest = CreateDiffController();

            long id = 1;

            DataModel leftData = new DataModel() { Data = "aW10aXJlZA==" };
            var leftResult = await unitUnderTest.CreateLeft(id, leftData);
            Assert.Equal(HttpStatusCode.Created, leftResult.StatusCode());

            DataModel rightData = new DataModel() { Data = "Y2FueW91cmVhZHRoaXM=" };
            var rightResult = await unitUnderTest.CreateRight(id, rightData);
            Assert.Equal(HttpStatusCode.Created, rightResult.StatusCode());

            var result = await unitUnderTest.GetDiff(id);
            Assert.Equal(typeof(OkObjectResult), result.GetType());

            DiffResultModel diffResult = (DiffResultModel)(result as OkObjectResult).Value;
            Assert.Equal(ResultType.SizeDoNotMatch, diffResult.ResultType);
            Assert.True(diffResult.Diffs.IsNullOrEmpty());
        }

        [Fact]
        public async Task CanGetDiffContentDoNotMatch()
        {
            var unitUnderTest = CreateDiffController();

            long id = 1;

            DataModel leftData = new DataModel() { Data = "MTIzNA==" }; // 1234
            var leftResult = await unitUnderTest.CreateLeft(id, leftData);
            Assert.Equal(HttpStatusCode.Created, leftResult.StatusCode());

            DataModel rightData = new DataModel() { Data = "NTIzNQ==" }; // 5235
            var rightResult = await unitUnderTest.CreateRight(id, rightData);
            Assert.Equal(HttpStatusCode.Created, rightResult.StatusCode());

            var result = await unitUnderTest.GetDiff(id);
            Assert.Equal(typeof(OkObjectResult), result.GetType());

            DiffResultModel diffResult = (DiffResultModel)(result as OkObjectResult).Value;
            Assert.Equal(ResultType.ContentDoNotMatch, diffResult.ResultType);
            Assert.NotEmpty(diffResult.Diffs);
            Assert.True(diffResult.Diffs.ToList().Count() == 2);
        }

        #endregion Testing to pass

        #region Testing to fail

        [Fact]
        public async Task CantGetDiffIfLeftAndRightDontExist()
        {
            var unitUnderTest = CreateDiffController();

            long id = 100;

            var result = await unitUnderTest.GetDiff(id);
            Assert.Equal(typeof(NotFoundResult), result.GetType());
        }

        [Fact]
        public async Task CantGetDiffIfLeftDoesntExist()
        {
            var unitUnderTest = CreateDiffController();

            long id = 105;
            DataModel data = new DataModel() { Data = "aW10aXJlZA==" };

            var createResult = await unitUnderTest.CreateLeft(id, data);
            Assert.Equal(HttpStatusCode.Created, createResult.StatusCode());

            var result = await unitUnderTest.GetDiff(id);
            Assert.Equal(typeof(NotFoundResult), result.GetType());
        }

        [Fact]
        public async Task CantGetDiffIfRightDoesntExist()
        {
            var unitUnderTest = CreateDiffController();

            long id = 110;
            DataModel data = new DataModel() { Data = "aW10aXJlZA==" };

            var createResult = await unitUnderTest.CreateRight(id, data);
            Assert.Equal(HttpStatusCode.Created, createResult.StatusCode());

            var result = await unitUnderTest.GetDiff(id);
            Assert.Equal(typeof(NotFoundResult), result.GetType());
        }

        [Fact]
        public async Task CantCreateNullLeft()
        {
            var unitUnderTest = CreateDiffController();

            long id = 1;
            DataModel data = new DataModel() { Data = null };

            var createResult = await unitUnderTest.CreateLeft(id, data);
            Assert.Equal(typeof(BadRequestObjectResult), createResult.GetType());
        }

        [Fact]
        public async Task CantCreateNullRight()
        {
            var unitUnderTest = CreateDiffController();

            long id = 1;
            DataModel data = new DataModel() { Data = null };

            var createResult = await unitUnderTest.CreateRight(id, data);
            Assert.Equal(typeof(BadRequestObjectResult), createResult.GetType());
        }

        [Fact]
        public async Task CantCreateNotBase64Left()
        {
            var unitUnderTest = CreateDiffController();

            long id = 1;
            DataModel data = new DataModel() { Data = "  1" };

            var createResult = await unitUnderTest.CreateLeft(id, data);
            Assert.Equal(typeof(BadRequestObjectResult), createResult.GetType());
        }

        [Fact]
        public async Task CantCreateNotBase64Right()
        {
            var unitUnderTest = CreateDiffController();

            long id = 1;
            DataModel data = new DataModel() { Data = "  1" };

            var createResult = await unitUnderTest.CreateRight(id, data);
            Assert.Equal(typeof(BadRequestObjectResult), createResult.GetType());
        }

        [Fact]
        public async Task CantUpdateLeftNull()
        {
            var unitUnderTest = CreateDiffController();

            long id = 1;
            DataModel data = new DataModel() { Data = "aW10aXJlZA==" };

            var result = await unitUnderTest.CreateLeft(id, data);
            Assert.Equal(HttpStatusCode.Created, result.StatusCode());

            DataModel updatedData = new DataModel() { Data = null };

            var updateResult = await unitUnderTest.CreateLeft(id, updatedData);
            Assert.Equal(typeof(BadRequestObjectResult), updateResult.GetType());
        }

        [Fact]
        public async Task CantUpdateRightNull()
        {
            var unitUnderTest = CreateDiffController();

            long id = 1;
            DataModel data = new DataModel() { Data = "aW10aXJlZA==" };

            var result = await unitUnderTest.CreateRight(id, data);
            Assert.Equal(HttpStatusCode.Created, result.StatusCode());

            DataModel updatedData = new DataModel() { Data = null };

            var updateResult = await unitUnderTest.CreateRight(id, updatedData);
            Assert.Equal(typeof(BadRequestObjectResult), updateResult.GetType());
        }

        [Fact]
        public async Task CantDeleteDiffResultAndDifferencesAfterFailedUpdate()
        {
            var unitUnderTest = CreateDiffController();

            // Create left and right data
            long id = 1;

            DataModel leftData = new DataModel() { Data = "dGhpcw==" }; // this
            var leftResult = await unitUnderTest.CreateLeft(id, leftData);
            Assert.Equal(HttpStatusCode.Created, leftResult.StatusCode());

            DataModel rightData = new DataModel() { Data = "dGhhdA==" }; // that
            var rightResult = await unitUnderTest.CreateRight(id, rightData);
            Assert.Equal(HttpStatusCode.Created, rightResult.StatusCode());

            // Calculate the difference and write it to the database
            var result = await unitUnderTest.GetDiff(id);
            Assert.Equal(typeof(OkObjectResult), result.GetType());

            DiffResultModel diffResult = (DiffResultModel)(result as OkObjectResult).Value;
            Assert.Equal(ResultType.ContentDoNotMatch, diffResult.ResultType);
            Assert.NotEmpty(diffResult.Diffs);
            Assert.True(diffResult.Diffs.ToList().Count() == 1);
            Assert.True(_context.DiffResults.Count() == 1);
            Assert.True(_context.Differences.Count() == 1);

            // Update left data, so it fails
            DataModel updatedData = new DataModel() { Data = null };

            var updateResult = await unitUnderTest.CreateLeft(id, updatedData);
            Assert.Equal(typeof(BadRequestObjectResult), updateResult.GetType());

            // Make sure that the Diff result and Differences were not removed from the database
            Assert.True(_context.DiffResults.Count() == 1);
            Assert.True(_context.Differences.Count() == 1);
        }

        #endregion Testing to fail
    }
}
