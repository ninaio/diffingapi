using DiffingApi.Models;
using DiffingApi.Models.Entities;
using DiffingApi.Models.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace DiffingApi.Tests.Unit.Models
{
    public class ExtensionsUnitTests
    {
        #region Testing to pass

        #region Method: IsNullOrEmpty(this IEnumerable<object> list)

        [Fact]
        public void CanDetermineListIsNull()
        {
            IEnumerable<string> testList = null;
            bool result = testList.IsNullOrEmpty();

            Assert.True(result == true);
        }

        [Fact]
        public void CanDetermineListIsEmpty()
        {
            List<Type> testList = new List<Type>();
            bool result = testList.IsNullOrEmpty();

            Assert.True(result == true);
        }

        [Fact]
        public void CanDetermineListIsNotEmpty()
        {
            List<object> testList = new List<object>() { 1, 2 };
            bool result = testList.IsNullOrEmpty();

            Assert.True(result == false);
        }

        #endregion Method: IsNullOrEmpty(this IEnumerable<object> list)

        #region Method: IsBase64String(this string base64String)

        [Fact]
        public void CanDetermineBase64StringIsNull()
        {
            string base64 = null;
            bool result = base64.IsBase64String();

            Assert.True(result == false);
        }

        [Fact]
        public void CanDetermineBase64StringIsWrongLength()
        {
            string base64 = "12345";
            bool result = base64.IsBase64String();

            Assert.True(result == false);
        }

        [Fact]
        public void CanDetermineBase64StringHasWrongChars()
        {
            string base64 = "****";
            bool result = base64.IsBase64String();

            Assert.True(result == false);
        }

        [Fact]
        public void CanDetermineIsProperBase64String()
        {
            string base64 = "1234";
            bool result = base64.IsBase64String();

            Assert.True(result == true);
        }

        #endregion Method: IsBase64String(this string base64String)

        #region Method: FindByDataIdAndSide(this IEnumerable<DataEntity> datas, long id, bool isLeft)

        [Fact]
        public void CanFindByDataIdAndSideLeft()
        {
            var result = _datas.FindByDataIdAndSide(1, true);
            Assert.True(result != null);
        }

        [Fact]
        public void CanFindByDataIdAndSideRight()
        {
            var result = _datas.FindByDataIdAndSide(1, false);
            Assert.True(result != null);
        }

        #endregion Method: FindByDataIdAndSide(this IEnumerable<DataEntity> datas, long id, bool isLeft)

        #region Mapping methods

        [Fact]
        public void CanMapDifferenceEntityListToDifferenceModelList()
        {
            List<DifferenceModel> modelList = _differences.MapToModel().ToList();

            bool result = true;

            foreach(var d in _differences)
            {
                if (modelList.Where(m => m.Length == d.Length && m.Offset == d.Offset).Count() != 1)
                {
                    result = false;
                }
            }

            Assert.True(result);
        }

        [Fact]
        public void CanMapDiffResultEntityToDiffResultModelWithoutDifferences()
        {
            DiffResultEntity entityToMap = new DiffResultEntity()
            {
                Id = 1,
                ResultType = ResultType.Equals
            };

            DiffResultModel model = entityToMap.MapToModel();

            bool result = model.Diffs.IsNullOrEmpty() 
                && entityToMap.Differences.IsNullOrEmpty()
                && model.ResultType == entityToMap.ResultType;

            Assert.True(result);
        }

        [Fact]
        public void CanMapDiffResultEntityToDiffResultModelWithDifferences()
        {
            DiffResultEntity entityToMap = new DiffResultEntity()
            {
                Id = 1,
                ResultType = ResultType.ContentDoNotMatch,
                Differences = _differences
            };

            DiffResultModel model = entityToMap.MapToModel();

            bool result = model.Diffs.ToList().Count() == entityToMap.Differences.Count
                && model.ResultType == entityToMap.ResultType;

            Assert.True(result);
        }

        #endregion Mapping methods

        #endregion Testing to pass

        #region Testing to fail

        [Fact]
        public void CantFindByDataIdAndSideWhenTableIsEmpty()
        {
            List<DataEntity> datas = new List<DataEntity>();
            var result = datas.FindByDataIdAndSide(1, false);
            Assert.True(result == null);
        }

        [Fact]
        public void CantFindByDataIdAndSideRight()
        {
            var result = _datas.FindByDataIdAndSide(2, false);
            Assert.True(result == null);
        }

        [Fact]
        public void CantFindByDataIdAndSideLeft()
        {
            var result = _datas.FindByDataIdAndSide(3, true);
            Assert.True(result == null);
        }

        #endregion Testing to fail

        #region Sample data

        private List<DifferenceEntity> _differences = new List<DifferenceEntity>()
        {
            new DifferenceEntity()
            {
                Id = 1,
                ResultId = 1,
                Length = 2,
                Offset = 0
            },
            new DifferenceEntity()
            {
                Id = 2,
                ResultId = 1,
                Length = 1,
                Offset = 4
            },
            new DifferenceEntity()
            {
                Id = 3,
                ResultId = 1,
                Length = 1,
                Offset = 6
            }
        };

        private List<DataEntity> _datas = new List<DataEntity>()
        {
            new DataEntity()
            {
                Id = 1,
                DataId = 1,
                IsLeft = true
            },
            new DataEntity()
            {
                Id = 2,
                DataId = 1,
                IsLeft = false
            },
            new DataEntity()
            {
                Id = 3,
                DataId = 2,
                IsLeft = true
            },
            new DataEntity()
            {
                Id = 4,
                DataId = 3,
                IsLeft = false
            }
        };

        #endregion Sample data
    }
}
