﻿using DiffingApi.Models;
using DiffingApi.Models.Enums;
using Newtonsoft.Json;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Xunit;

namespace DiffingApi.Tests.Integration
{
    public class DiffControllerIntegrationTests : IClassFixture<CustomWebApplicationFactory<Startup>>
    {
        private readonly HttpClient _client;

        public DiffControllerIntegrationTests(CustomWebApplicationFactory<Startup> factory)
        {
            _client = factory.CreateClient();
        }

        #region Testing to pass

        [Fact]
        public async Task CanCreateLeft()
        {
            DataModel left = new DataModel() { Data = "SSB3YW50IHRoaXMgam9i" };
            var createLeftResponse = await _client.PutAsJsonAsync<DataModel>("/v1/diff/1/left", left);
            Assert.Equal(HttpStatusCode.Created, createLeftResponse.StatusCode);
        }

        [Fact]
        public async Task CanCreateRight()
        {
            DataModel right = new DataModel() { Data = "aGVsbG8=" };
            var createRightResponse = await _client.PutAsJsonAsync<DataModel>("/v1/diff/1/right", right);
            Assert.Equal(HttpStatusCode.Created, createRightResponse.StatusCode);
        }

        [Fact]
        public async Task CanUpdateLeft()
        {
            DataModel left = new DataModel() { Data = "SSB3YW50IHRoaXMgam9i" };
            var createLeftResponse = await _client.PutAsJsonAsync<DataModel>("/v1/diff/1/left", left);
            Assert.Equal(HttpStatusCode.Created, createLeftResponse.StatusCode);

            DataModel updatedLeft = new DataModel() { Data = "aGVsbG8=" };
            var updateLeftResponse = await _client.PutAsJsonAsync<DataModel>("/v1/diff/1/left", updatedLeft);
            Assert.Equal(HttpStatusCode.Created, updateLeftResponse.StatusCode);
        }

        [Fact]
        public async Task CanUpdateRight()
        {
            DataModel right = new DataModel() { Data = "aGVsbG8=" };
            var createRightResponse = await _client.PutAsJsonAsync<DataModel>("/v1/diff/1/right", right);
            Assert.Equal(HttpStatusCode.Created, createRightResponse.StatusCode);

            DataModel updatedRight = new DataModel() { Data = "SSB3YW50IHRoaXMgam9i" };
            var updateRightResponse = await _client.PutAsJsonAsync<DataModel>("/v1/diff/1/right", updatedRight);
            Assert.Equal(HttpStatusCode.Created, updateRightResponse.StatusCode);
        }

        [Fact]
        public async Task CanGetDiffEqual()
        {
            DataModel left = new DataModel() { Data = "SSB3YW50IHRoaXMgam9i" };
            var createLeftResponse = await _client.PutAsJsonAsync<DataModel>("/v1/diff/1/left", left);
            Assert.Equal(HttpStatusCode.Created, createLeftResponse.StatusCode);

            DataModel right = new DataModel() { Data = "SSB3YW50IHRoaXMgam9i" };
            var createRightResponse = await _client.PutAsJsonAsync<DataModel>("/v1/diff/1/right", right);
            Assert.Equal(HttpStatusCode.Created, createRightResponse.StatusCode);

            var getDiffResponse = await _client.GetAsync("/v1/diff/1");
            Assert.Equal(HttpStatusCode.OK, getDiffResponse.StatusCode);

            DiffResultModel result = JsonConvert.DeserializeObject<DiffResultModel>(await getDiffResponse.Content.ReadAsStringAsync());
            Assert.Equal(ResultType.Equals, result.ResultType);
            Assert.True(result.Diffs.IsNullOrEmpty());
        }

        [Fact]
        public async Task CanGetDiffSizeDoNotMatch()
        {
            DataModel left = new DataModel() { Data = "SSB3YW50IHRoaXMgam9i" };
            var createLeftResponse = await _client.PutAsJsonAsync<DataModel>("/v1/diff/1/left", left);
            Assert.Equal(HttpStatusCode.Created, createLeftResponse.StatusCode);

            DataModel right = new DataModel() { Data = "aGVsbG8=" };
            var createRightResponse = await _client.PutAsJsonAsync<DataModel>("/v1/diff/1/right", right);
            Assert.Equal(HttpStatusCode.Created, createRightResponse.StatusCode);

            var getDiffResponse = await _client.GetAsync("/v1/diff/1");
            Assert.Equal(HttpStatusCode.OK, getDiffResponse.StatusCode);

            DiffResultModel result = JsonConvert.DeserializeObject<DiffResultModel>(await getDiffResponse.Content.ReadAsStringAsync());
            Assert.Equal(ResultType.SizeDoNotMatch, result.ResultType);
            Assert.True(result.Diffs.IsNullOrEmpty());
        }

        [Fact]
        public async Task CanGetDiffContentDoNotMatch()
        {
            DataModel left = new DataModel() { Data = "aGVvb28=" }; // heooo
            var createLeftResponse = await _client.PutAsJsonAsync<DataModel>("/v1/diff/1/left", left);
            Assert.Equal(HttpStatusCode.Created, createLeftResponse.StatusCode);

            DataModel right = new DataModel() { Data = "aGVsbG8=" }; // hello
            var createRightResponse = await _client.PutAsJsonAsync<DataModel>("/v1/diff/1/right", right);
            Assert.Equal(HttpStatusCode.Created, createRightResponse.StatusCode);

            var getDiffResponse = await _client.GetAsync("/v1/diff/1");
            Assert.Equal(HttpStatusCode.OK, getDiffResponse.StatusCode);

            DiffResultModel result = JsonConvert.DeserializeObject<DiffResultModel>(await getDiffResponse.Content.ReadAsStringAsync());
            Assert.Equal(ResultType.ContentDoNotMatch, result.ResultType);
            Assert.NotEmpty(result.Diffs);
            Assert.True(result.Diffs.Count() == 1);
        }

        #endregion Testing to pass

        #region Testing to fail

        [Fact]
        public async Task CantGetDiffIfLeftAndRightDontExist()
        {
            var getDiffResponse = await _client.GetAsync("/v1/diff/100");
            Assert.Equal(HttpStatusCode.NotFound, getDiffResponse.StatusCode);
        }

        [Fact]
        public async Task CantGetDiffIfLeftDoesntExist()
        {
            DataModel right = new DataModel() { Data = "aGVsbG8=" };
            var createRightResponse = await _client.PutAsJsonAsync<DataModel>("/v1/diff/105/right", right);
            Assert.Equal(HttpStatusCode.Created, createRightResponse.StatusCode);

            var getDiffResponse = await _client.GetAsync("/v1/diff/105");
            Assert.Equal(HttpStatusCode.NotFound, getDiffResponse.StatusCode);
        }

        [Fact]
        public async Task CantGetDiffIfRightDoesntExist()
        {
            DataModel left = new DataModel() { Data = "SSB3YW50IHRoaXMgam9i" };
            var createLeftResponse = await _client.PutAsJsonAsync<DataModel>("/v1/diff/110/left", left);
            Assert.Equal(HttpStatusCode.Created, createLeftResponse.StatusCode);

            var getDiffResponse = await _client.GetAsync("/v1/diff/110");
            Assert.Equal(HttpStatusCode.NotFound, getDiffResponse.StatusCode);
        }

        [Fact]
        public async Task CantCreateNullLeft()
        {
            DataModel left = new DataModel() { Data = null };
            var createLeftNullResponse = await _client.PutAsJsonAsync<DataModel>("/v1/diff/1/left", left);
            Assert.Equal(HttpStatusCode.BadRequest, createLeftNullResponse.StatusCode);
        }

        [Fact]
        public async Task CantCreateNullRight()
        {
            DataModel right = new DataModel() { Data = null };
            var createRightNullResponse = await _client.PutAsJsonAsync<DataModel>("/v1/diff/1/right", right);
            Assert.Equal(HttpStatusCode.BadRequest, createRightNullResponse.StatusCode);
        }

        [Fact]
        public async Task CantCreateNotBase64Left()
        {
            DataModel left = new DataModel() { Data = "***" };
            var createLeftNullResponse = await _client.PutAsJsonAsync<DataModel>("/v1/diff/1/left", left);
            Assert.Equal(HttpStatusCode.BadRequest, createLeftNullResponse.StatusCode);
        }

        [Fact]
        public async Task CantCreateNotBase64Right()
        {
            DataModel right = new DataModel() { Data = "***" };
            var createRightNullResponse = await _client.PutAsJsonAsync<DataModel>("/v1/diff/1/right", right);
            Assert.Equal(HttpStatusCode.BadRequest, createRightNullResponse.StatusCode);
        }

        [Fact]
        public async Task CantUpdateLeftNull()
        {
            DataModel left = new DataModel() { Data = "SSB3YW50IHRoaXMgam9i" };
            var createLeftResponse = await _client.PutAsJsonAsync<DataModel>("/v1/diff/1/left", left);
            Assert.Equal(HttpStatusCode.Created, createLeftResponse.StatusCode);

            DataModel updatedLeft = new DataModel() { Data = null };
            var updateLeftResponse = await _client.PutAsJsonAsync<DataModel>("/v1/diff/1/left", updatedLeft);
            Assert.Equal(HttpStatusCode.BadRequest, updateLeftResponse.StatusCode);
        }

        [Fact]
        public async Task CantUpdateRightNull()
        {
            DataModel right = new DataModel() { Data = "aGVsbG8=" };
            var createRightResponse = await _client.PutAsJsonAsync<DataModel>("/v1/diff/1/right", right);
            Assert.Equal(HttpStatusCode.Created, createRightResponse.StatusCode);

            DataModel updatedRight = new DataModel() { Data = null };
            var updateRightResponse = await _client.PutAsJsonAsync<DataModel>("/v1/diff/1/right", updatedRight);
            Assert.Equal(HttpStatusCode.BadRequest, updateRightResponse.StatusCode);
        }

        #endregion Testing to fail
    }
}
