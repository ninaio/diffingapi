﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc.Testing;
using Microsoft.Extensions.Configuration;

namespace DiffingApi.Tests.Integration
{
    /// <summary>
    /// A class derived from DiffingApi.Startup class that 
    /// sets IsInTestMode to true in constructor and instructs
    /// the api to use in memory database
    /// Used by Integration tests as an entry point
    /// </summary>
    public class TestStartup : Startup
    {
        public TestStartup(IConfiguration configuration)
            : base(configuration, true)
        {
        }
    }

    public class CustomWebApplicationFactory<TStartup> : WebApplicationFactory<Startup>
    {
        protected override void ConfigureWebHost(IWebHostBuilder builder)
        {
            // Define TestStartup as an entry point
            builder.UseStartup<TestStartup>();
        }
    }
}
